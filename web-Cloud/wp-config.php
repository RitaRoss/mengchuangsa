<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'testwordpress' );

/** Database username */
define( 'DB_USER', 'testwordpress' );

/** Database password */
define( 'DB_PASSWORD', 'testwordpress' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'H+bW5IHgB5Jld; ?SY(hnXkS:suafGD]v!ylKyoGLvg89^WuYD$EW*r6jOSPIQjt' );
define( 'SECURE_AUTH_KEY',  '=I7h3#Z/NJiC@i>5k1h|5i/I?Hfjz{x]W]mq5&$a}e{sjaXnQ6cW3x$e*@MwJtRq' );
define( 'LOGGED_IN_KEY',    'Wyi2GBQ/v<4^7E<I-|#3?d(<@>_5>2LNe6YGEwhYTQZlD-PLD<7Q[J{TOZEfV9+m' );
define( 'NONCE_KEY',        'IVkj-yx=4$@cj!A72d-;qzhbr;H8|el2IX,#/fil((.5/6o.l_GL?VBv>DlkC(ox' );
define( 'AUTH_SALT',        'HyToS0vy$&zzW~@,~,@;8IJ-=E#w25.J78(=wICeb$o_wPJU4Svi7K8yz7})#0^ ' );
define( 'SECURE_AUTH_SALT', '(hXcDVieO?#m$:@G?[jmiQ$l4f[_7?%y`_y#5ZXa0z~qON9(h<g5W6~!;$oN/Y)5' );
define( 'LOGGED_IN_SALT',   'js@LS+sliUqN9e<T<A4+f8qNL(FgK>K1k|6vwMUl>]4774R?G/9Fp}#+C>Vp3jY~' );
define( 'NONCE_SALT',       '# uGibf0lUaObo5/]Z:[(_}.X%GrQd!5cu?$F+.sPKM_~ch]v|L-V5thlR[^,puj' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */

/** 允许上传任意类型附件 */
define('ALLOW_UNFILTERED_UPLOADS', true);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
