﻿namespace MissionPlanner.GCSViews.ConfigurationView
{
    partial class ConfigFirmwareMC
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigFirmwareMC));
            this.imageLabelcopter = new MissionPlanner.Controls.ImageLabel();
            this.labelcopter = new System.Windows.Forms.Label();
            this.imageLabelrover = new MissionPlanner.Controls.ImageLabel();
            this.imageLabelAntennaTracker = new MissionPlanner.Controls.ImageLabel();
            this.imageLabelPlane = new MissionPlanner.Controls.ImageLabel();
            this.imageLabelsolo = new MissionPlanner.Controls.ImageLabel();
            this.imageLabelSub = new MissionPlanner.Controls.ImageLabel();
            this.labelrover = new System.Windows.Forms.Label();
            this.labelAntennaTracker = new System.Windows.Forms.Label();
            this.labelplane = new System.Windows.Forms.Label();
            this.labelsolo = new System.Windows.Forms.Label();
            this.labelsub = new System.Windows.Forms.Label();
            this.download_fm = new System.Windows.Forms.Button();
            this.cus_fm = new System.Windows.Forms.Button();
            this.progress = new System.Windows.Forms.ProgressBar();
            this.lbl_status = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // imageLabelcopter
            // 
            this.imageLabelcopter.Image = ((System.Drawing.Image)(resources.GetObject("imageLabelcopter.Image")));
            this.imageLabelcopter.Location = new System.Drawing.Point(35, 18);
            this.imageLabelcopter.Name = "imageLabelcopter";
            this.imageLabelcopter.Size = new System.Drawing.Size(132, 140);
            this.imageLabelcopter.TabIndex = 0;
            this.imageLabelcopter.Click += new System.EventHandler(this.imageLabelcopter_Click);
            // 
            // labelcopter
            // 
            this.labelcopter.AutoSize = true;
            this.labelcopter.Location = new System.Drawing.Point(73, 152);
            this.labelcopter.Name = "labelcopter";
            this.labelcopter.Size = new System.Drawing.Size(52, 15);
            this.labelcopter.TabIndex = 6;
            this.labelcopter.Text = "多旋翼";
            // 
            // imageLabelrover
            // 
            this.imageLabelrover.Image = ((System.Drawing.Image)(resources.GetObject("imageLabelrover.Image")));
            this.imageLabelrover.Location = new System.Drawing.Point(225, 18);
            this.imageLabelrover.Name = "imageLabelrover";
            this.imageLabelrover.Size = new System.Drawing.Size(132, 140);
            this.imageLabelrover.TabIndex = 1;
            this.imageLabelrover.Click += new System.EventHandler(this.imageLabelrover_Click);
            // 
            // imageLabelAntennaTracker
            // 
            this.imageLabelAntennaTracker.Image = ((System.Drawing.Image)(resources.GetObject("imageLabelAntennaTracker.Image")));
            this.imageLabelAntennaTracker.Location = new System.Drawing.Point(412, 18);
            this.imageLabelAntennaTracker.Name = "imageLabelAntennaTracker";
            this.imageLabelAntennaTracker.Size = new System.Drawing.Size(132, 140);
            this.imageLabelAntennaTracker.TabIndex = 2;
            this.imageLabelAntennaTracker.Click += new System.EventHandler(this.imageLabelAntennaTracker_Click);
            // 
            // imageLabelPlane
            // 
            this.imageLabelPlane.Image = ((System.Drawing.Image)(resources.GetObject("imageLabelPlane.Image")));
            this.imageLabelPlane.Location = new System.Drawing.Point(35, 236);
            this.imageLabelPlane.Name = "imageLabelPlane";
            this.imageLabelPlane.Size = new System.Drawing.Size(132, 140);
            this.imageLabelPlane.TabIndex = 3;
            this.imageLabelPlane.Click += new System.EventHandler(this.imageLabelPlane_Click);
            // 
            // imageLabelsolo
            // 
            this.imageLabelsolo.Image = ((System.Drawing.Image)(resources.GetObject("imageLabelsolo.Image")));
            this.imageLabelsolo.Location = new System.Drawing.Point(225, 236);
            this.imageLabelsolo.Name = "imageLabelsolo";
            this.imageLabelsolo.Size = new System.Drawing.Size(132, 140);
            this.imageLabelsolo.TabIndex = 4;
            this.imageLabelsolo.Click += new System.EventHandler(this.imageLabelsolo_Click);
            // 
            // imageLabelSub
            // 
            this.imageLabelSub.Image = ((System.Drawing.Image)(resources.GetObject("imageLabelSub.Image")));
            this.imageLabelSub.Location = new System.Drawing.Point(412, 236);
            this.imageLabelSub.Name = "imageLabelSub";
            this.imageLabelSub.Size = new System.Drawing.Size(132, 140);
            this.imageLabelSub.TabIndex = 5;
            this.imageLabelSub.Click += new System.EventHandler(this.imageLabelSub_Click);
            // 
            // labelrover
            // 
            this.labelrover.AutoSize = true;
            this.labelrover.Location = new System.Drawing.Point(262, 152);
            this.labelrover.Name = "labelrover";
            this.labelrover.Size = new System.Drawing.Size(52, 15);
            this.labelrover.TabIndex = 7;
            this.labelrover.Text = "巡航车";
            // 
            // labelAntennaTracker
            // 
            this.labelAntennaTracker.AutoSize = true;
            this.labelAntennaTracker.Location = new System.Drawing.Point(438, 152);
            this.labelAntennaTracker.Name = "labelAntennaTracker";
            this.labelAntennaTracker.Size = new System.Drawing.Size(82, 15);
            this.labelAntennaTracker.TabIndex = 8;
            this.labelAntennaTracker.Text = "天线跟踪器";
            // 
            // labelplane
            // 
            this.labelplane.AutoSize = true;
            this.labelplane.Location = new System.Drawing.Point(80, 375);
            this.labelplane.Name = "labelplane";
            this.labelplane.Size = new System.Drawing.Size(37, 15);
            this.labelplane.TabIndex = 9;
            this.labelplane.Text = "飞机";
            // 
            // labelsolo
            // 
            this.labelsolo.AutoSize = true;
            this.labelsolo.Location = new System.Drawing.Point(248, 375);
            this.labelsolo.Name = "labelsolo";
            this.labelsolo.Size = new System.Drawing.Size(84, 15);
            this.labelsolo.TabIndex = 10;
            this.labelsolo.Text = "solo无人机";
            // 
            // labelsub
            // 
            this.labelsub.AutoSize = true;
            this.labelsub.Location = new System.Drawing.Point(453, 375);
            this.labelsub.Name = "labelsub";
            this.labelsub.Size = new System.Drawing.Size(52, 15);
            this.labelsub.TabIndex = 11;
            this.labelsub.Text = "潜水器";
            // 
            // download_fm
            // 
            this.download_fm.Location = new System.Drawing.Point(598, 44);
            this.download_fm.Name = "download_fm";
            this.download_fm.Size = new System.Drawing.Size(100, 50);
            this.download_fm.TabIndex = 12;
            this.download_fm.Text = "下载固件";
            this.download_fm.UseVisualStyleBackColor = true;
            this.download_fm.Click += new System.EventHandler(this.download_fm_Click);
            // 
            // cus_fm
            // 
            this.cus_fm.Location = new System.Drawing.Point(598, 109);
            this.cus_fm.Name = "cus_fm";
            this.cus_fm.Size = new System.Drawing.Size(100, 50);
            this.cus_fm.TabIndex = 13;
            this.cus_fm.Text = "烧写固件";
            this.cus_fm.UseVisualStyleBackColor = true;
            this.cus_fm.Click += new System.EventHandler(this.cus_fm_Click);
            // 
            // progress
            // 
            this.progress.Location = new System.Drawing.Point(35, 425);
            this.progress.Name = "progress";
            this.progress.Size = new System.Drawing.Size(509, 23);
            this.progress.TabIndex = 14;
            // 
            // lbl_status
            // 
            this.lbl_status.AutoSize = true;
            this.lbl_status.Location = new System.Drawing.Point(32, 460);
            this.lbl_status.Name = "lbl_status";
            this.lbl_status.Size = new System.Drawing.Size(37, 15);
            this.lbl_status.TabIndex = 15;
            this.lbl_status.Text = "状态";
            // 
            // ConfigFirmwareMC
            // 
            this.Controls.Add(this.lbl_status);
            this.Controls.Add(this.progress);
            this.Controls.Add(this.cus_fm);
            this.Controls.Add(this.download_fm);
            this.Controls.Add(this.labelsub);
            this.Controls.Add(this.labelsolo);
            this.Controls.Add(this.labelplane);
            this.Controls.Add(this.labelAntennaTracker);
            this.Controls.Add(this.labelrover);
            this.Controls.Add(this.imageLabelSub);
            this.Controls.Add(this.imageLabelsolo);
            this.Controls.Add(this.imageLabelPlane);
            this.Controls.Add(this.imageLabelAntennaTracker);
            this.Controls.Add(this.imageLabelrover);
            this.Controls.Add(this.labelcopter);
            this.Controls.Add(this.imageLabelcopter);
            this.Name = "ConfigFirmwareMC";
            this.Size = new System.Drawing.Size(764, 526);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Controls.ImageLabel imageLabelcopter;
        private System.Windows.Forms.Label labelcopter;
        private Controls.ImageLabel imageLabelrover;
        private Controls.ImageLabel imageLabelAntennaTracker;
        private Controls.ImageLabel imageLabelPlane;
        private Controls.ImageLabel imageLabelsolo;
        private Controls.ImageLabel imageLabelSub;
        private System.Windows.Forms.Label labelrover;
        private System.Windows.Forms.Label labelAntennaTracker;
        private System.Windows.Forms.Label labelplane;
        private System.Windows.Forms.Label labelsolo;
        private System.Windows.Forms.Label labelsub;
        private System.Windows.Forms.Button download_fm;
        private System.Windows.Forms.Button cus_fm;
        private System.Windows.Forms.ProgressBar progress;
        private System.Windows.Forms.Label lbl_status;
    }
}
