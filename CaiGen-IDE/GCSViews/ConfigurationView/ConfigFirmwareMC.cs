﻿using log4net;
using MissionPlanner.Controls;
using MissionPlanner.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MissionPlanner.GCSViews.ConfigurationView;


namespace MissionPlanner.GCSViews.ConfigurationView
{
    public partial class ConfigFirmwareMC : MyUserControl
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static List<Firmware.software> softwares = new List<Firmware.software>();
        private readonly Firmware fw = new Firmware();
        private string custom_fw_dir = Settings.Instance["FirmwareFileDirectory"] ?? "";
        private string firmwareurl = "";
        private bool firstrun = true;
        private IProgressReporterDialogue pdr;
        private string detectedport;

        public ConfigFirmwareMC()
        {
            InitializeComponent();
        }

        private void imageLabelcopter_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://47.122.26.3/download/226/?tmstv=1689153954");
            }
            catch
            {
                CustomMessageBox.Show("Failed to open url http://47.122.26.3/download/226/?tmstv=1689153954");
            }
        }

        private void imageLabelrover_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://47.122.26.3/download/228/?tmstv=1689153998");
            }
            catch
            {
                CustomMessageBox.Show("Failed to open url http://47.122.26.3/download/228/?tmstv=1689153998");
            }
        }

        private void imageLabelAntennaTracker_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://47.122.26.3/download/230/?tmstv=1689154016");
            }
            catch
            {
                CustomMessageBox.Show("Failed to open url http://47.122.26.3/download/230/?tmstv=1689154016");
            }
        }

        private void imageLabelPlane_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://47.122.26.3/download/232/?tmstv=1689154043");
            }
            catch
            {
                CustomMessageBox.Show("Failed to open url http://47.122.26.3/download/232/?tmstv=1689154043");
            }
        }

        private void imageLabelsolo_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://47.122.26.3/download/234/?tmstv=1689154052");
            }
            catch
            {
                CustomMessageBox.Show("Failed to open url http://47.122.26.3/download/234/?tmstv=1689154052");
            }
        }

        private void imageLabelSub_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://47.122.26.3/download/236/?tmstv=1689154061");
            }
            catch
            {
                CustomMessageBox.Show("Failed to open url http://47.122.26.3/download/236/?tmstv=1689154061");
            }
        }

        private void download_fm_Click(object sender, EventArgs e)
        {
            try
            {
                Process.Start("http://47.122.26.3/download/26/?tmstv=1688029787");
            }
            catch
            {
                CustomMessageBox.Show("Can not open url http://47.122.26.3/download/26/?tmstv=1688029787", Strings.ERROR);
            }
        }

        /// <summary>
        ///     for updating fw list
        /// </summary>
        /// <param name="progress"></param>
        /// <param name="status"></param>
        private void fw_ProgressPDR(int progress, string status)
        {
            pdr.UpdateProgressAndStatus(progress, status);
        }

        /// <summary>
        ///     for when updating fw to hardware
        /// </summary>
        /// <param name="progress"></param>
        /// <param name="status"></param>
        private void fw_Progress1(int progress, string status)
        {
            this.BeginInvokeIfRequired(() =>
            {
                var change = false;

                if (progress != -1)
                {
                    if (this.progress.Value != progress)
                    {
                        this.progress.Value = progress;
                        change = true;
                    }
                }

                if (lbl_status.Text != status)
                {
                    lbl_status.Text = status;
                    change = true;
                }

                if (change)
                    this.Refresh();
            });
        }
        public static Func<List<ArduPilot.DeviceInfo>> ExtraDeviceInfo;
        private void cus_fm_Click(object sender, EventArgs e)
        {
            using (var fd = new OpenFileDialog
            { Filter = "Firmware (*.hex;*.px4;*.vrx;*.apj)|*.hex;*.px4;*.vrx;*.apj|All files (*.*)|*.*" })
            {
                if (Directory.Exists(custom_fw_dir))
                    fd.InitialDirectory = custom_fw_dir;
                fd.ShowDialog();
                if (File.Exists(fd.FileName))
                {
                    custom_fw_dir = Path.GetDirectoryName(fd.FileName);
                    Settings.Instance["FirmwareFileDirectory"] = custom_fw_dir;

                    fw.Progress -= fw_ProgressPDR;
                    fw.Progress += fw_Progress1;

                    var boardtype = BoardDetect.boards.none;
                    try
                    {
                        if (fd.FileName.ToLower().EndsWith(".px4") || fd.FileName.ToLower().EndsWith(".apj"))
                        {
                            if (solo.Solo.is_solo_alive &&
                                CustomMessageBox.Show("Solo", "Is this a Solo?",
                                    CustomMessageBox.MessageBoxButtons.YesNo) == CustomMessageBox.DialogResult.Yes)
                            {
                                boardtype = BoardDetect.boards.solo;
                            }
                            else
                            {
                                boardtype = BoardDetect.boards.px4v2;
                            }
                        }
                        else
                        {
                            var ports = Win32DeviceMgmt.GetAllCOMPorts();
                            ports.AddRange(Linux.GetAllCOMPorts());

                            if (ExtraDeviceInfo != null)
                            {
                                try
                                {
                                    ports.AddRange(ExtraDeviceInfo.Invoke());
                                }
                                catch
                                {

                                }
                            }

                            boardtype = BoardDetect.DetectBoard(MainV2.comPortName, ports);
                        }

                        if (boardtype == BoardDetect.boards.none)
                        {
                            CustomMessageBox.Show(Strings.CantDetectBoardVersion);
                            return;
                        }
                    }
                    catch
                    {
                        CustomMessageBox.Show(Strings.CanNotConnectToComPortAnd, Strings.ERROR);
                        return;
                    }

                    try
                    {
                        fw.UploadFlash(MainV2.comPortName, fd.FileName, boardtype);
                    }
                    catch (Exception ex)
                    {
                        CustomMessageBox.Show(ex.ToString(), Strings.ERROR);
                    }
                }
            }
        }
    }
}
