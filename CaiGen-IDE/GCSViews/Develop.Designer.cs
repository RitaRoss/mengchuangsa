﻿namespace MissionPlanner.GCSViews
{
    partial class Develop
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Develop));
            this.BUT_Cloud = new MissionPlanner.Controls.MyButton();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openfile = new MissionPlanner.Controls.MyButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rtb = new System.Windows.Forms.RichTextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ButtonBold = new System.Windows.Forms.ToolStripButton();
            this.ButtonUnderLine = new System.Windows.Forms.ToolStripButton();
            this.ButtonLtalic = new System.Windows.Forms.ToolStripButton();
            this.ButtonLeft = new System.Windows.Forms.ToolStripButton();
            this.ButtonCenter = new System.Windows.Forms.ToolStripButton();
            this.ButtonRight = new System.Windows.Forms.ToolStripButton();
            this.LabelColor = new System.Windows.Forms.ToolStripLabel();
            this.ButtonColor = new System.Windows.Forms.ToolStripButton();
            this.LabelTextsize = new System.Windows.Forms.ToolStripLabel();
            this.ComboBoxTextcolor = new System.Windows.Forms.ToolStripComboBox();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.savefile = new MissionPlanner.Controls.MyButton();
            this.ButtonStore = new MissionPlanner.Controls.MyButton();
            this.sendfile = new MissionPlanner.Controls.MyButton();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BUT_Cloud
            // 
            resources.ApplyResources(this.BUT_Cloud, "BUT_Cloud");
            this.BUT_Cloud.Name = "BUT_Cloud";
            this.BUT_Cloud.UseVisualStyleBackColor = true;
            this.BUT_Cloud.Click += new System.EventHandler(this.BUT_Cloud_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // openfile
            // 
            resources.ApplyResources(this.openfile, "openfile");
            this.openfile.Name = "openfile";
            this.openfile.UseVisualStyleBackColor = true;
            this.openfile.Click += new System.EventHandler(this.openfile_Click);
            // 
            // panel1
            // 
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Controls.Add(this.progressBar);
            this.panel1.Controls.Add(this.rtb);
            this.panel1.Controls.Add(this.toolStrip1);
            this.panel1.Name = "panel1";
            // 
            // rtb
            // 
            resources.ApplyResources(this.rtb, "rtb");
            this.rtb.Name = "rtb";
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.ButtonBold,
            this.ButtonUnderLine,
            this.ButtonLtalic,
            this.ButtonLeft,
            this.ButtonCenter,
            this.ButtonRight,
            this.LabelColor,
            this.ButtonColor,
            this.LabelTextsize,
            this.ComboBoxTextcolor});
            resources.ApplyResources(this.toolStrip1, "toolStrip1");
            this.toolStrip1.Name = "toolStrip1";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
            // 
            // ButtonBold
            // 
            this.ButtonBold.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.ButtonBold, "ButtonBold");
            this.ButtonBold.Name = "ButtonBold";
            this.ButtonBold.Click += new System.EventHandler(this.ButtonBold_Click);
            // 
            // ButtonUnderLine
            // 
            this.ButtonUnderLine.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.ButtonUnderLine, "ButtonUnderLine");
            this.ButtonUnderLine.Name = "ButtonUnderLine";
            this.ButtonUnderLine.Click += new System.EventHandler(this.ButtonUnderLine_Click);
            // 
            // ButtonLtalic
            // 
            this.ButtonLtalic.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.ButtonLtalic, "ButtonLtalic");
            this.ButtonLtalic.Name = "ButtonLtalic";
            this.ButtonLtalic.Click += new System.EventHandler(this.ButtonLtalic_Click);
            // 
            // ButtonLeft
            // 
            this.ButtonLeft.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.ButtonLeft, "ButtonLeft");
            this.ButtonLeft.Margin = new System.Windows.Forms.Padding(20, 1, 0, 2);
            this.ButtonLeft.Name = "ButtonLeft";
            this.ButtonLeft.Click += new System.EventHandler(this.ButtonLeft_Click);
            // 
            // ButtonCenter
            // 
            this.ButtonCenter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.ButtonCenter, "ButtonCenter");
            this.ButtonCenter.Name = "ButtonCenter";
            this.ButtonCenter.Click += new System.EventHandler(this.ButtonCenter_Click);
            // 
            // ButtonRight
            // 
            this.ButtonRight.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.ButtonRight, "ButtonRight");
            this.ButtonRight.Name = "ButtonRight";
            this.ButtonRight.Click += new System.EventHandler(this.ButtonRight_Click);
            // 
            // LabelColor
            // 
            this.LabelColor.ForeColor = System.Drawing.SystemColors.ControlText;
            this.LabelColor.Margin = new System.Windows.Forms.Padding(20, 1, 0, 2);
            this.LabelColor.Name = "LabelColor";
            resources.ApplyResources(this.LabelColor, "LabelColor");
            // 
            // ButtonColor
            // 
            this.ButtonColor.BackColor = System.Drawing.SystemColors.Control;
            this.ButtonColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this.ButtonColor, "ButtonColor");
            this.ButtonColor.Name = "ButtonColor";
            this.ButtonColor.Click += new System.EventHandler(this.ButtonColor_Click);
            // 
            // LabelTextsize
            // 
            this.LabelTextsize.ForeColor = System.Drawing.SystemColors.ControlText;
            this.LabelTextsize.Margin = new System.Windows.Forms.Padding(20, 1, 0, 2);
            this.LabelTextsize.Name = "LabelTextsize";
            resources.ApplyResources(this.LabelTextsize, "LabelTextsize");
            // 
            // ComboBoxTextcolor
            // 
            this.ComboBoxTextcolor.BackColor = System.Drawing.SystemColors.Window;
            this.ComboBoxTextcolor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxTextcolor.Items.AddRange(new object[] {
            resources.GetString("ComboBoxTextcolor.Items"),
            resources.GetString("ComboBoxTextcolor.Items1"),
            resources.GetString("ComboBoxTextcolor.Items2"),
            resources.GetString("ComboBoxTextcolor.Items3"),
            resources.GetString("ComboBoxTextcolor.Items4"),
            resources.GetString("ComboBoxTextcolor.Items5"),
            resources.GetString("ComboBoxTextcolor.Items6"),
            resources.GetString("ComboBoxTextcolor.Items7"),
            resources.GetString("ComboBoxTextcolor.Items8"),
            resources.GetString("ComboBoxTextcolor.Items9"),
            resources.GetString("ComboBoxTextcolor.Items10"),
            resources.GetString("ComboBoxTextcolor.Items11"),
            resources.GetString("ComboBoxTextcolor.Items12"),
            resources.GetString("ComboBoxTextcolor.Items13"),
            resources.GetString("ComboBoxTextcolor.Items14"),
            resources.GetString("ComboBoxTextcolor.Items15"),
            resources.GetString("ComboBoxTextcolor.Items16"),
            resources.GetString("ComboBoxTextcolor.Items17"),
            resources.GetString("ComboBoxTextcolor.Items18"),
            resources.GetString("ComboBoxTextcolor.Items19"),
            resources.GetString("ComboBoxTextcolor.Items20"),
            resources.GetString("ComboBoxTextcolor.Items21"),
            resources.GetString("ComboBoxTextcolor.Items22"),
            resources.GetString("ComboBoxTextcolor.Items23")});
            this.ComboBoxTextcolor.Name = "ComboBoxTextcolor";
            resources.ApplyResources(this.ComboBoxTextcolor, "ComboBoxTextcolor");
            this.ComboBoxTextcolor.DropDown += new System.EventHandler(this.ComboBoxTextsize_DropDown);
            this.ComboBoxTextcolor.SelectedIndexChanged += new System.EventHandler(this.ComboBoxTextsize_SelectedIndexChanged);
            // 
            // savefile
            // 
            resources.ApplyResources(this.savefile, "savefile");
            this.savefile.Name = "savefile";
            this.savefile.UseVisualStyleBackColor = true;
            this.savefile.Click += new System.EventHandler(this.savefile_Click);
            // 
            // ButtonStore
            // 
            resources.ApplyResources(this.ButtonStore, "ButtonStore");
            this.ButtonStore.Name = "ButtonStore";
            this.ButtonStore.UseVisualStyleBackColor = true;
            this.ButtonStore.Click += new System.EventHandler(this.ButtonStore_Click);
            // 
            // sendfile
            // 
            resources.ApplyResources(this.sendfile, "sendfile");
            this.sendfile.Name = "sendfile";
            this.sendfile.UseVisualStyleBackColor = true;
            this.sendfile.Click += new System.EventHandler(this.sendfile_Click);
            // 
            // progressBar
            // 
            resources.ApplyResources(this.progressBar, "progressBar");
            this.progressBar.Name = "progressBar";
            // 
            // Develop
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.Controls.Add(this.sendfile);
            this.Controls.Add(this.ButtonStore);
            this.Controls.Add(this.savefile);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.openfile);
            this.Controls.Add(this.BUT_Cloud);
            resources.ApplyResources(this, "$this");
            this.Name = "Develop";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Controls.MyButton BUT_Cloud;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private Controls.MyButton openfile;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RichTextBox rtb;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton ButtonBold;
        private System.Windows.Forms.ToolStripButton ButtonUnderLine;
        private System.Windows.Forms.ToolStripButton ButtonLtalic;
        private System.Windows.Forms.ToolStripButton ButtonLeft;
        private System.Windows.Forms.ToolStripButton ButtonCenter;
        private System.Windows.Forms.ToolStripButton ButtonRight;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.ToolStripLabel LabelColor;
        private System.Windows.Forms.ToolStripButton ButtonColor;
        private System.Windows.Forms.ToolStripLabel LabelTextsize;
        private System.Windows.Forms.ToolStripComboBox ComboBoxTextcolor;
        private Controls.MyButton savefile;
        private Controls.MyButton ButtonStore;
        private Controls.MyButton sendfile;
        private System.Windows.Forms.ProgressBar progressBar;
    }
}
