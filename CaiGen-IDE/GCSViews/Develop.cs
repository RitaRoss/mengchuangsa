﻿using Microsoft.CodeAnalysis.CSharp.Syntax;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MissionPlanner.GCSViews
{
    public partial class Develop : MyUserControl
    {
        public Develop()
        {
            InitializeComponent();
            ComboBoxTextcolor.SelectedIndex = 11;
            this.ButtonColor.BackColor = colorDialog1.Color;
        }
        
        private void BUT_Cloud_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("http://47.122.26.3/%e7%bc%96%e8%af%91%e5%9b%ba%e4%bb%b6/");
            }
            catch
            {
                CustomMessageBox.Show("Failed to open url http://47.122.26.3/%e7%bc%96%e8%af%91%e5%9b%ba%e4%bb%b6/");
            }
        }

        private void ButtonBold_Click(object sender, EventArgs e)
        {
            Font oldFont;
            Font newFont;

            oldFont = this.rtb.SelectionFont;

            if (oldFont.Bold)
                newFont = new Font(oldFont, oldFont.Style & ~FontStyle.Bold);
            else
                newFont = new Font(oldFont, oldFont.Style | FontStyle.Bold);

            this.rtb.SelectionFont = newFont;
            this.rtb.Focus();
        }

        private void ButtonUnderLine_Click(object sender, EventArgs e)
        {
            Font oldFont;
            Font newFont;

            oldFont = this.rtb.SelectionFont;

            if (oldFont.Underline)
                newFont = new Font(oldFont, oldFont.Style & ~FontStyle.Underline);
            else
                newFont = new Font(oldFont, oldFont.Style | FontStyle.Underline);

            this.rtb.SelectionFont = newFont;
            this.rtb.Focus();
        }

        private void ButtonLtalic_Click(object sender, EventArgs e)
        {
            Font oldFont;
            Font newFont;

            oldFont = this.rtb.SelectionFont;

            if (oldFont.Italic)
                newFont = new Font(oldFont, oldFont.Style & ~FontStyle.Italic);
            else
                newFont = new Font(oldFont, oldFont.Style | FontStyle.Italic);

            this.rtb.SelectionFont = newFont;
            this.rtb.Focus();
        }

        private void ButtonLeft_Click(object sender, EventArgs e)
        {
            if (this.rtb.SelectionAlignment == HorizontalAlignment.Left)
                this.rtb.SelectionAlignment = HorizontalAlignment.Center;
            else
                this.rtb.SelectionAlignment = HorizontalAlignment.Left;
            this.rtb.Focus();
        }

        private void ButtonCenter_Click(object sender, EventArgs e)
        {
            if (this.rtb.SelectionAlignment == HorizontalAlignment.Center)
                this.rtb.SelectionAlignment = HorizontalAlignment.Left;
            else
                this.rtb.SelectionAlignment = HorizontalAlignment.Center;
            this.rtb.Focus();
        }

        private void ButtonRight_Click(object sender, EventArgs e)
        {
            if (this.rtb.SelectionAlignment == HorizontalAlignment.Right)
                this.rtb.SelectionAlignment = HorizontalAlignment.Center;
            else
                this.rtb.SelectionAlignment = HorizontalAlignment.Right;
            this.rtb.Focus();
        }
        
        private void ButtonColor_Click(object sender, EventArgs e)
        {
            this.rtb.Focus();
            // 打开颜色选择对话框 ,并分析是否选择了对话框中的确定按钮 
            this.colorDialog1.ShowDialog();
            // 将先中的颜色设置为窗体的背景色
            this.rtb.SelectionColor = colorDialog1.Color;
            this.ButtonColor.BackColor = colorDialog1.Color;
        }

        private void ComboBoxTextsize_DropDown(object sender, EventArgs e)
        {
            this.rtb.Focus();
        }

        private void ComboBoxTextsize_SelectedIndexChanged(object sender, EventArgs e)
        {
            ToolStripComboBox txt = (ToolStripComboBox)sender;
            ApplyTextSize(txt.Text);
            this.rtb.Focus();
        }

        private void ApplyTextSize(string textSize)
        {
            float newSize = Convert.ToSingle(textSize);
            FontFamily currentFontFamily;
            Font newFont;
            currentFontFamily = this.rtb.SelectionFont.FontFamily;
            newFont = new Font(currentFontFamily, newSize);

            this.rtb.SelectionFont = newFont;
        }
        string pname;
        private void OpenFromFile()
        {
            //openFileDialog1.Filter = "txt格式（*.txt）|*.txt|所有文件|*.*";
            //openFileDialog1.Title = "打开";
            DialogResult dr = this.openFileDialog1.ShowDialog();
            if (dr == DialogResult.OK)
            {
                pname = this.openFileDialog1.FileName;
                rtb.Clear();//先clear再显示新的内容
                rtb.LoadFile(openFileDialog1.FileName, RichTextBoxStreamType.PlainText); //打开      
                rtb.Show();//显示
            }
            /*openFileDialog1.FileName = pname;//要打开的文件的路径
            rtb.Clear();//先clear再显示新的内容
            rtb.LoadFile(openFileDialog1.FileName, RichTextBoxStreamType.PlainText); //打开      
            rtb.Show();//显示*/

        }

        private void SaveToFile()
        {
            //saveFileDialog.InitialDirectory = pname;//设置保存的默认目录
            saveFileDialog1.FileName = pname;
            //saveFileDialog1.Filter = "txt files(*.txt)|*.txt|all files(*.*)|*.*";
            //saveFileDialog1.FilterIndex = 1;//默认显示保存类型为TXT
            saveFileDialog1.RestoreDirectory = true;
            rtb.SaveFile(saveFileDialog1.FileName, RichTextBoxStreamType.PlainText);
        }

        private void openfile_Click(object sender, EventArgs e)
        {
            OpenFromFile();
        }

        private void savefile_Click(object sender, EventArgs e)
        {
            SaveToFile();
        }

        private void ButtonStore_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("https://shop144519723.taobao.com/?spm=2013.1.1000126.3.4bb66a40pzQLyE");
            }
            catch
            {
                CustomMessageBox.Show("Failed to open url https://shop144519723.taobao.com/?spm=2013.1.1000126.3.4bb66a40pzQLyE");
            }
        }

        private void sendfile_Click(object sender, EventArgs e)
        {
            String SFTP_IP = System.Configuration.ConfigurationManager.AppSettings["SFTP_IP"];
            String SFTP_port = System.Configuration.ConfigurationManager.AppSettings["SFTP_port"];
            String SFTP_username = System.Configuration.ConfigurationManager.AppSettings["SFTP_username"];
            String SFTP_password = System.Configuration.ConfigurationManager.AppSettings["SFTP_password"];
            String SFTP_catalog = System.Configuration.ConfigurationManager.AppSettings["SFTP_catalog"];
            

            String route = "";
            String name = "";
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;//该值确定是否可以选择多个文件
            dialog.Title = "请选择文件";
            //dialog.Filter = "所有文件(*.csv)|*.csv";
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                name = System.IO.Path.GetFileName(dialog.FileName);//得到文件名
                route = System.IO.Path.GetFullPath(dialog.FileName);//绝对路径
                //textBox1.Text = name;
            }
            FileInfo f = new FileInfo(route);
            String uploadfile = f.FullName;

            //1.SFTP建立连接           
            var client = new SftpClient(SFTP_IP, Int32.Parse(SFTP_port), SFTP_username, SFTP_password);
            client.Connect();
            if (client.IsConnected)
            {
                Console.WriteLine("I AM CONNECTED");
            }
            var fileStream = new FileStream(uploadfile, FileMode.Open);
            if (fileStream != null)
            {
                Console.WriteLine("YOU ARE NOT NULL");
            }
            client.BufferSize = 4 * 1024;
            client.UploadFile(fileStream, SFTP_catalog + f.Name, progress =>
            {
                // Update progress bar based on 'progress' value
                int percentage = (int)((double)progress / fileStream.Length * 100);
                progressBar.Value = percentage;
            });
            client.Disconnect();
            MessageBox.Show("文件上传完成！");
            client.Dispose();
        }
    }
}
